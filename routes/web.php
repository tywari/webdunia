<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/create_person','HomeController@createPerson');

Route::post('/create_relative','HomeController@createRelative');

Route::get('/person_list','HomeController@getPersonList');

Route::post('/get_relative','HomeController@getPersonRelativeList');