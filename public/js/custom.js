TOOLS = {};
TOOLS.Loader = '<i class="fa fa-cog fa-spin" aria-hidden="true"></i>';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function showSuccessNotification(message) {

    setTimeout(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr.success(message, 'Alert');

    }, 100);
}

function showErrorNotification(message) {

    setTimeout(function () {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr.error(message, 'Alert');

    }, 100);
}


$(document).ready(function () {

    // $("#createPersonForm").validate({
    //     rules: {
    //         name: {
    //             required: true
    //         },
    //         age: {
    //             required: true
    //         },
    //         city: {
    //             required: true
    //         },
    //         state: {
    //             required: true
    //         }
    //     },
    //     messages: {
    //         name: {
    //             required: 'Person name can not be blank'
    //         },
    //         age: {
    //             required: 'Person age can not be blank'
    //         },
    //         city: {
    //             required: 'Person city can not be blank'
    //         },
    //         state: {
    //             required: 'Person state can not be blank'
    //         }
    //     }
    // });

    $(document).on('click', '#createPerson', function () {

            var _this = $(this);
            TOOLS.dt = _this.html();
            $.ajax({
                type: 'post',
                url: CONF.baseurl + '/create_person',
                dataType: 'html',
                data: $("#createPersonForm").serialize(),
                beforeSend: function () {
                    _this.html(TOOLS.Loader).attr('disabled', true);
                },
                success: function (response) {
                    $("#person").hide();
                    $("#relation").html(response);

                },
                complete: function () {
                    _this.html(TOOLS.dt).removeAttr('disabled');
                }
            })

    });

    $(document).on('click', '#createRelative', function () {

        var _this = $(this);
        TOOLS.dt = _this.html();
        $.ajax({
            type: 'post',
            url: CONF.baseurl + '/create_relative',
            dataType: 'json',
            data: $("#createRelationForm").serialize(),
            beforeSend: function () {
                _this.html(TOOLS.Loader).attr('disabled', true);
            },
            success: function (response) {
                if (response.status == 'SUCC')
                {
                    $("#relation").hide();
                    $("#result").removeClass('hide');
                    showSuccessNotification(response.message);
                }else
                {
                    showErrorNotification(response.message);
                }


            },
            complete: function () {
                _this.html(TOOLS.dt).removeAttr('disabled');
            }
        })

    });

    $(document).on('click','.person-name', function(){
        var person_id = $(this).data('id');

        var _this = $(this);
        TOOLS.dt = _this.html();
        $.ajax({
            type: 'post',
            url: CONF.baseurl + '/get_relative',
            dataType: 'html',
            data: {'_token':$('meta[name="csrf-token"]').attr('content'),'person_id':person_id},
            beforeSend: function () {
                _this.html(TOOLS.dt + TOOLS.Loader).attr('disabled', true);
            },
            success: function (response) {
               $("#familyList").html(response);
            },
            complete: function () {
                _this.html(TOOLS.dt).removeAttr('disabled');
            }
        })
    });

});
