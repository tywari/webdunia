<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationModel extends Model
{
    protected $table = 'relatives';
    protected $fillable = ['person_id','relation_id','relative_name'];
}
