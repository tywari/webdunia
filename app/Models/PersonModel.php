<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonModel extends Model
{
    protected $table = 'person';
    protected $fillable = ['name','age','city','state'];

    public static function getRelativeList($person_id)
    {
         $list = PersonModel::join('relatives', 'person.id', '=', 'relatives.person_id')
            ->join('relation_type','relatives.relation_id','=','relation_type.id')
            ->select('relatives.id','relatives.relative_name as name','relation_type.name as relation')
            ->where('relatives.person_id','=',$person_id)
            ->get();
         if ($list)
         {
             return $list;
         }else
         {
             return false;
         }
    }
}
