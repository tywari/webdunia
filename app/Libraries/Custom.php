<?php
/**
 * Created by PhpStorm.
 * User: anuj
 * Date: 19/04/18
 * Time: 12:11 AM
 */
namespace App\Libraries;

use Ixudra\Curl\Facades\Curl;

class Custom
{
    public static function callPostApi($url,$data)
    {
        return $response = Curl::to($url)
            ->withData($data)
            ->asJson(true)
            ->enableDebug('/var/www/logFile.txt')
            ->post();
    }
}