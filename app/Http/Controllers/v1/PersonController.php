<?php

namespace App\Http\Controllers\v1;

use App\Models\RelationModel;
use App\Traits\CustomResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PersonModel;
use App\Models\RelationTypeModel;

class PersonController extends Controller
{
    use CustomResponse;

    public function __construct()
    {
      $this->middleware('apiAuth');
    }

    public function createPerson(Request $request)
    {
        $data = [
            'name' => $request['name'],
            'age'  => $request['age'],
            'city'  => $request['city'],
            'state'  => $request['state'],
        ];
        $create = PersonModel::create($data);
        if ($create)
        {
            $relation = RelationTypeModel::where(['status' =>  1])->get();
            return $this->_status('SUCC','',['relation' => $relation,'person_id' => $create->id]);
        }else
        {
            return $this->_status('ERR','');
        }

    }

    public function createRelative(Request $request)
    {
        $data = [
            'person_id'    => $request['person_id'],
            'relation_id'     => $request['relation_id'],
            'relative_name'    => $request['relative_name']
        ];
        $create = RelationModel::create($data);
        if ($create)
        {
            return $this->_status('SUCC','');
        }else
        {
            return $this->_status('ERR','');
        }

    }

    public function getPersonList(Request $request)
    {
        $list = PersonModel::all();
        if ($list)
        {
            return $this->_status('SUCC','',$list);
        }else
        {
            return $this->_status('ERR','');
        }

    }

    public function getPersonRelativeList(Request $request)
    {
        $person_id = $request['person_id'];
        $checkPerson = PersonModel::find($person_id);
        if ($checkPerson)
        {
            $getRelative = PersonModel::getRelativeList($person_id);
            if ($getRelative)
            {
                return $this->_status('SUCC','',$getRelative);

            }else
            {
                return $this->_status('ERR','');

            }
        }else
        {
            return $this->_status('ERR','Invalid Person id');
        }

    }
}
