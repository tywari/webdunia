<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ixudra\Curl\Facades\Curl;
use App\Libraries\Custom;
use App\Models\RelationTypeModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->apiUrl = env('API_URL');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function createPerson(Request $request)
    {
        $data = [
             'user_id' => Auth::user()->id,
             'name'    => $request['name'],
             'age'     => $request['age'],
             'city'    => $request['city'],
             'state'   => $request['state'],
        ];

        $url = $this->apiUrl.'/createPerson';
        $response = Custom::callPostApi($url,$data);
        if ($response['status'] == 'SUCC')
        {
            return view('relation',['data' => $response['data']['relation'], 'person_id' => $response['data']['person_id']]);
        }
    }

    public function createRelative(Request $request)
    {
        $data = [
             'user_id' => Auth::user()->id,
             'person_id'    => $request['person_id'],
             'relation_id'     => $request['relation_id'],
             'relative_name'    => $request['relativeName']
        ];

        $url = $this->apiUrl.'/createRelative';
       return $response = Custom::callPostApi($url,$data);

    }

    public function getPersonList()
    {
        $data = ['user_id' => Auth::user()->id];
        $url = $this->apiUrl.'/getPersonList';
        $response = Custom::callPostApi($url,$data);
        if ($response['status'] == 'SUCC')
        {
            return view('person',['list' => $response['data']]);
        }
    }

    public function getPersonRelativeList(Request $request)
    {
        $data = [
            'user_id' => Auth::user()->id,
            'person_id' => $request['person_id']
        ];
        $url = $this->apiUrl.'/getPersonRelativeList';
        $response = Custom::callPostApi($url,$data);
        if ($response['status'] == 'SUCC')
        {
            return view('relative_list',['list' => $response['data']]);
        }
    }
}
