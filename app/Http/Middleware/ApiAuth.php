<?php

namespace App\Http\Middleware;

use Closure;
use App\Traits\CustomResponse;
use App\User;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    use CustomResponse;

    public function handle($request, Closure $next)
    {
         $authId = $request->input('user_id');
        if ((isset($authId))) {
                $logout = User::where(['id' => $authId])->first();
                if (!$logout) {
                    $response = $this->_status('UAD', 'unauthorized access', (object)[]);
                    echo json_encode($response);
                    exit;
                }
            } else {
                $response = $this->_status('UAD', 'invalid headers', (object)[]);
                echo json_encode($response);
                exit;
            }
        return $next($request);
    }
}
