<?php
/**
 * Created by IntelliJ IDEA.
 * User: vivek
 * Date: 16/9/17
 * Time: 12:56 PM
 */

namespace App\Traits;


trait CustomResponse
{
    public function _status($key = "", $msg = "", $data = "") {
        $d = "ERR";
        switch ($key):
            case 'SUCC':$d = 'Request Successfully Accepted';
                break;

            case 'FAIL':$d = 'Request Failed';
                break;

            case 'ERR':$d = 'Unknown Error';
                break;

            default:$d = '';
        endswitch;

        if ($d == '') {
            $d = $key;
            $key = "ERR";
        }

        $res["status"] = $key;
        $res["message"]     = $d;

        if ($msg != "") {
            $res["message"] = $msg;
        }

        $res["data"] = $data;

        return $res;
    }

}
