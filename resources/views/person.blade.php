@extends('layouts.app')

@section('header')
    <link href="{{ URL::asset('/public/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/public/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/public/css/toastr.min.css')}}" rel="stylesheet">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        </style>
    @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" id="personList">
                <div class="card-header">Create New Person</div>
                @csrf
                <div class="card-body">
                    @if(!empty($list))
                        <table>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Age</th>
                                <th>City</th>
                                <th>State</th>
                            </tr>
                            @php
                            $i = 1;
                            @endphp
                            @foreach($list as $key => $value)
                            <tr>
                                <td>{{$i}}</td>
                                <td><a href="javascript:void(0);" class="person-name" data-id="{{$value['id']}}">{{$value['name']}}</a></td>
                                <td>{{$value['age']}}</td>
                                <td>{{$value['city']}}</td>
                                <td>{{$value['state']}}</td>
                                @php
                                $i++
                                @endphp
                            </tr>
                                @endforeach
                        </table>
                        @else
                        <h2> No Record Found!</h2>
                    @endif
                </div>
            </div>
            <div class="card" id="familyList">

            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ URL::asset('/public/js/jquery-3.1.1.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('/public/js/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('/public/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('/public/js/custom.js')}}"></script>
@endsection
