<div class="card-header">Create New Relation</div>
<div class="card-body">

    <form method="POST" action="" name="createRelationForm" id="createRelationForm">
        @csrf
        <div class="form-group row">
            <label for="relation_id" class="col-sm-4 col-form-label text-md-right">Select Relation</label>
            <div class="col-md-6">
            <select id="relation_id" name="relation_id" class="form-control">
                <option value="">Select option</option>
                @foreach($data as  $items)
                    <option value="{{$items['id']}}">{{$items['name']}}</option>
                @endforeach
            </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-sm-4 col-form-label text-md-right">Relative Name</label>
            <div class="col-md-6">
                <input id="relativeName" type="text" class="form-control" name="relativeName" value="" required autofocus>
            </div>
        </div>
        <input type="hidden" id="person_id" name="person_id" value="{{$person_id}}">
        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="button" class="btn btn-primary" name="createRelative" id="createRelative">
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>
