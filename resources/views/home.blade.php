@extends('layouts.app')

@section('header')
    <link href="{{ URL::asset('/public/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('/public/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('/public/css/toastr.min.css')}}" rel="stylesheet">
    @endsection
    <style>
        .caret
        {
            display: none !important;
        }
    </style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" id="person">
                <div class="card-header">Create New Person</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                        <form method="POST" action="" name="createPersonForm" id="createPersonForm">
                            @csrf
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right">Person Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right">Person Age</label>
                                <div class="col-md-6">
                                    <input id="age" type="text" class="form-control" name="age" value="" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right">Person City</label>
                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control" name="city" value="" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right">Person State</label>
                                <div class="col-md-6">
                                    <input id="state" type="text" class="form-control" name="state" value="" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="button" class="btn btn-primary" name="createPerson" id="createPerson">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
            <div class="card" id="relation"></div>

            <div class="card hide" id="result">
                <div>
                    <h3>Relative Creation Successful.</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{ URL::asset('/public/js/jquery-3.1.1.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('/public/js/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('/public/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('/public/js/custom.js')}}"></script>
@endsection
