
<div class="card-header">Relative List</div>
<div class="card-body">
    @if(!empty($list))
        <table>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Relation</th>
            </tr>
            @php
                $i = 1;
            @endphp
            @foreach($list as $key => $value)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{$value['name']}}</td>
                    <td>{{$value['relation']}}</td>
                    @php
                        $i++
                    @endphp
                </tr>
            @endforeach
        </table>
    @else
        <h2> No Record Found!</h2>
    @endif
</div>